-- phpMyAdmin SQL Dump
-- version 3.5.2
-- http://www.phpmyadmin.net
--
-- Host: internal-db.s106349.gridserver.com
-- Generation Time: Jul 25, 2014 at 11:35 PM
-- Server version: 5.1.67-rel14.3
-- PHP Version: 5.3.27

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `db106349_fitness`
--

-- --------------------------------------------------------

--
-- Table structure for table `albums`
--

CREATE TABLE IF NOT EXISTS `albums` (
  `AlbumId` int(11) NOT NULL AUTO_INCREMENT,
  `AlbumName` varchar(100) NOT NULL,
  `AlbumAuthor` int(5) NOT NULL,
  `AlbumCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`AlbumId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `CategoryId` int(5) NOT NULL AUTO_INCREMENT,
  `CategoryName` varchar(100) NOT NULL,
  PRIMARY KEY (`CategoryId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
  `ContactId` int(11) NOT NULL AUTO_INCREMENT,
  `ContactName` varchar(100) NOT NULL,
  `ContactEmail` varchar(50) NOT NULL,
  `ContactPhone` varchar(20) NOT NULL,
  `ContactMessage` text NOT NULL,
  `ContactCategory` int(5) NOT NULL,
  `ContactCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ContactId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `newsletter`
--

CREATE TABLE IF NOT EXISTS `newsletter` (
  `SubscriberId` int(6) NOT NULL AUTO_INCREMENT,
  `SubscriberName` varchar(100) NOT NULL,
  `SubscriberEmail` varchar(50) NOT NULL,
  `SubscriberCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`SubscriberId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE IF NOT EXISTS `photos` (
  `PhotoId` int(11) NOT NULL AUTO_INCREMENT,
  `PhotoFileName` varchar(100) NOT NULL,
  `PhotoAlbum` int(11) NOT NULL,
  `PhotoCaption` tinytext NOT NULL,
  `PhotoAuthor` int(11) NOT NULL,
  `PhotoCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`PhotoId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `PostId` int(11) NOT NULL AUTO_INCREMENT,
  `PostTitle` varchar(200) NOT NULL,
  `PostContent` text NOT NULL,
  `PostCategory` int(5) NOT NULL,
  `PostImage` int(11) NOT NULL,
  `PostTags` tinytext NOT NULL,
  `PostAuthor` int(11) NOT NULL,
  `PostCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`PostId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE IF NOT EXISTS `products` (
  `ProductId` int(11) NOT NULL AUTO_INCREMENT,
  `ProductTitle` varchar(100) NOT NULL,
  `ProductContent` text NOT NULL,
  `ProductCategory` int(5) NOT NULL,
  `ProductImage` int(11) NOT NULL,
  `ProductAuthor` int(11) NOT NULL,
  `ProductTags` text NOT NULL,
  `ProductCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`ProductId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='could be projects, case studies, products, etc' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `UserId` int(6) NOT NULL AUTO_INCREMENT,
  `Fname` varchar(50) NOT NULL,
  `Lname` varchar(50) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Pass` varchar(100) NOT NULL,
  `UserImage` varchar(100) NOT NULL,
  `FacebookId` varchar(20) NOT NULL,
  `APNSToken` varchar(200) NOT NULL,
  `UserCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`UserId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `_registrations`
--

CREATE TABLE IF NOT EXISTS `_registrations` (
  `RegisterId` int(11) NOT NULL AUTO_INCREMENT,
  `RegisterFName` varchar(100) NOT NULL,
  `RegisterLname` varchar(100) NOT NULL,
  `RegisterEmail` varchar(50) NOT NULL,
  `RegisterPhone``` varchar(20) NOT NULL,
  `RegisterCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`RegisterId`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='a custom table for handling fitness registrations' AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
