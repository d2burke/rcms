# rCMS  - A CMS builts on a RESTful API, no GUI necessary #
rCMS is a RESTful interface for managing users, blog posts, newsletters, photo albums and more, all via a simple & secure API.

## Summary
This project is meant to let developers build sites and applications on top of a content management system without the bloat of imposed design templates.  Although rCMS is written in PHP, it's interface is strictly RESTful which means that using is is language agnostic.  Write your client in Js, C#, Java, Objective C, for web or native applications.  

## Version 0.5

## API
### EndPoints
 * #### users - typical user management
 * #### albums - collections of photos
 * #### photos - uploaded photos
 * #### posts - blog posts, reviews, etc
 * #### products - products, projects, case studies, etc
 * #### subscribers - newsletter subscribers
 * #### contacts - contact form submissions
 * #### categories - single-word categories to help manage site content
 * #### roles - user roles (Admin, Writer, Subscriber, Reader)
 * #### auth - handles user authentication
 * #### menus - menu items based on parent/child structure
 * #### settings - cms management settings

### Envelopes
###### TRUE
 * `{'status':TRUE, 'users':[]}`
 * `{'status':TRUE, 'subscribers':[
        {'UserId':1},
        {'UserId':4}
    ]}`
###### FALSE
 * `{'status':FALSE}`

### Status Codes
* 200 - A good request that returned at least one record
* 204 - A good request that returned zero records
* 304 - No records where updated
* 400 - You've sent some bad parameters, or the values of those parameters didn't pass validation
* 500 - Some sort of server error has occurred.  This will happen if there is some sort of database error

## Setup
### Download & Installation
Download and unzip rCMS and upload it's contents to a folder of your choice. Edit the included `.htaccess` file on line 2 to point to the folder where you've placed these files e.g.

    RewriteBase /dev/cms

* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

## Pull Request Guidelines ##

* Writing tests
* Code review

## Questions? ##

Daniel Burke [website](http://www.d2burke.com)