<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Users_model extends CI_Model {

	public function __construct(){
		parent::__construct();
		
	}
	
	/*
	 *	If a UserId is passed, filter by that and
	 *	only send one user back, otherwise, send all
	 */
	function get_users($UserId=NULL){
		if($UserId){
			$this->db->where('UserId', $UserId);
		}
		$this->db->join('photos', 'users.UserImage = photos.PhotoId', 'LEFT');
		$users = $this->db->get('users');
		if($users && $users->num_rows() > 0){
			return $users->result();
		}
	}

	/*
	 *	Validation is done in the Controller, trust and
	 *	insert parameters
	 */
	function create_user($params){
		$created = $this->db->insert('users', $params);
		if($created){
			return $this->db->insert_id();
		}
	}

	/*
	 *	Update user with supplied parameters
	 */
	function update_user($UserId, $params){
		$this->db->where('UserId', $UserId);
		$updated = $this->db->update('users', $params);
		// echo json_encode($this->db->last_query());
		if($updated){
			if(!$this->db->affected_rows()){
				return 304;
			}
			return TRUE;
		}
		else{
			return 500;
		}
	}

	/*
	 *
	 */
	function delete_user($UserId){
		$this->db->where('UserId', $UserId);
		$deleted = $this->db->delete('users');
		if($deleted){
			if(!$this->db->affected_rows()){
				require 304;
			}
			return TRUE;
		}
		else{
			return 500;
		}
	}
}