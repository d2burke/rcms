<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Utility_model extends CI_Model{
	
	public function __construct(){
		parent::__construct();
		
	}

	/*
	 *	Reusable function to check provided parameters against
	 *	allowed parameters
	 */
	function checkValidParams($params, $allowed){
		foreach($params as $key=>$val){
			$key = (is_numeric($key)) ? $val : $key;
			if(!in_array($key, $allowed)){
				return FALSE;
			}
		}
		return $params;
	}
}