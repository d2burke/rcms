<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Menus_model extends CI_Model {

	public function __construct(){
		parent::__construct();
		
	}
	
	/*
	 *	If a ProductId is passed, filter by that and
	 *	only send one user back, otherwise, send all
	 */
	function get_menus($MenuId=NULL){
		if($MenuId){
			$this->db->where('MenuId', $MenuId);
		}

		$menus = $this->db->get('menus');
		if($menus && $menus->num_rows() > 0){
			return $menus->result();
		}
	}

	function create_menu($params){
		$created = $this->db->insert('menus', $params);
		if($created){
			return $this->db->insert_id();
		}
	}

	/*
	 *
	 */
	function delete_menu($MenuId){
		$this->db->where('MenuId', $MenuId);
		$deleted = $this->db->delete('menus');
		if($deleted){
			if(!$this->db->affected_rows()){
				return 304;
			}
			return TRUE;
		}
		else{
			return 500;
		}
	}
}