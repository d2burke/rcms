<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Photos_model extends CI_Model {

	public function __construct(){
		parent::__construct();
		
	}

	function get_photos($params=array(), $fields=array()){
		if(count($params)){
			$this->db->where($params);
		}
		$photos = $this->db->get('photos');
		if($photos && $photos->num_rows() > 0){
			return $photos->result();
		}
	}

	/*
	 *	@param $fieldName: Form field name of the item being uploaded
	 */
	function upload_photo($fieldName, $fileName=NULL){
		$config['upload_path'] = 'uploads/images/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']	= '10000';
		$config['max_width'] = '5000';
		$config['max_height'] = '5000';
		if($fileName){
			$config['file_name'] = $fileName;
		}
		$this->load->library('upload', $config);
		$this->upload->do_upload($fieldName);
		$data = $this->upload->data();
		$errors = $this->upload->display_errors();
		if(!$errors){
			return $data;
		}
	}

	/*
	 *
	 */
	function create_photo($params){
		$fileName = NULL;
		if(isset($params['PhotoFileName'])){
			$fileName = $this->security->sanitize_filename($params['PhotoFileName']);
		}
		$photoData = $this->upload_photo('ImageUpload', $fileName);
		if($photoData){
			$params['PhotoFileName'] = $photoData['file_name'];
			$created = $this->db->insert('photos', $params);
			if($created){
				return $this->db->insert_id();
			}
		}
	}

	/*
	 *
	 */
	function delete_photo($PhotoId){
		$this->db->where('PhotoId', $PhotoId);
		$deleted = $this->db->delete('photos');
		if($deleted){
			if(!$this->db->affected_rows()){
				require 304;
			}
			return TRUE;
		}
		else{
			return 500;
		}
	}
}