<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Products_model extends CI_Model {

	public function __construct(){
		parent::__construct();
		
	}
	
	/*
	 *	If a ProductId is passed, filter by that and
	 *	only send one user back, otherwise, send all
	 */
	function get_products($ProductId=NULL){
		if($ProductId){
			$this->db->where('ProductId', $ProductId);
		}

		$products = $this->db->get('products');
		if($products && $products->num_rows() > 0){
			return $products->result();
		}
	}

	function create_product($params){
		$created = $this->db->insert('products', $params);
		if($created){
			return $this->db->insert_id();
		}
	}

	/*
	 *
	 */
	function delete_product($ProductId){
		$this->db->where('ProductId', $ProductId);
		$deleted = $this->db->delete('products');
		if($deleted){
			if(!$this->db->affected_rows()){
				return 304;
			}
			return TRUE;
		}
		else{
			return 500;
		}
	}
}