<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Roles_model extends CI_Model {

	public function __construct(){
		parent::__construct();
	}

	function get_roles(){
		$this->db->order_by('RoleId');
		$roles = $this->db->get('roles');
		if($roles && $roles->num_rows()){
			return $roles->result();
		}
	}

	/*
	 *	Validation is done in the Controller, trust and
	 *	insert parameters
	 */
	function create_role($params){
		$created = $this->db->insert('roles', $params);
		if($created){
			return $this->db->insert_id();
		}
	}

	function delete_role($RoleId){
		$this->db->where('RoleId', $RoleId);
		$deleted = $this->db->delete('roles');
		if($deleted){
			if(!$this->db->affected_rows()){
				require 304;
			}
			return TRUE;
		}
		else{
			return 500;
		}
	}
}