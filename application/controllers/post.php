<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Post extends REST_Controller
{
	function __construct(){
		parent::__construct();
		$this->load->model('Posts_model', 'posts');
		$this->load->model('Photos_model', 'photos');
 		$this->load->model('Utility_model', 'util');
	}

	/*
	 *	Get all posts, or pass filters
	 *	TODO: Pagination
	 */
	function posts_get(){
		$filters = $this->input->get();
		$PostId = $this->uri->segment(2);
		if($filters){
			$allowed_filters = array('PostId','PostTitle','PostSubtitle','PostCategory','PostTags','PostAuthor','fields','callback','_');
			$validFilters = $this->util->checkValidParams($filters, $allowed_filters);
			if(!$validFilters){
				$this->response(array('status'=>FALSE), 400);
			}
			else{
				//TODO: Move to a parameter on get_users()
				if(isset($validFilters['fields'])) unset($validFilters['fields']);
				if(isset($validFilters['callback'])) unset($validFilters['callback']);
				if(isset($validFilters['_'])) unset($validFilters['_']);
				$this->db->where($validFilters);
			}
		}

		$fields = $this->input->get('fields');
		if($fields){
			$fields = explode(',',$fields);
			$allowed_fields = array('PostId','PostTitle','PostSubtitle','PostContent','PostCategory','CategoryName','PostImage','PhotoFileName','PostTags','PostAuthor','PostCreated');
			$validFields = $this->util->checkValidParams($fields, $allowed_fields);
			if(!$validFields){
				$this->response(array('status'=>FALSE), 400);
			}
			else{
				//TODO: Move this to a parameter on get_users()
				$this->db->select($validFields);
			}
		}

		$posts = $this->posts->get_posts($PostId);
		if($posts){
			$this->response(array('status'=>TRUE, 'posts'=>$posts), 200);
		}
		else{
			$this->response(array('status'=>TRUE, 'posts'=>'No Posts'), 204);	
		}
	}

	/*
	 *	Check for unwanted parameters, then validation their values.
	 *	If these tests pass, and call the 
	 *	posts model to insert the data.
	 *
	 *	If they fail, send a Malformed Request code.
	 */
	function posts_post(){
		$allowed = array('PostTitle','PostSubtitle','PostContent','PostCategory','PostImage','PostTags','PostAuthor');
		$params = $this->input->post();
		$validParams = $this->util->checkValidParams($params, $allowed);
		if(!$validParams){
			$this->response(array('status'=>$params), 400);	
		}

		$this->load->library('form_validation');
		$this->form_validation->set_rules('PostTitle', 'Post Title', 'required|xss_clean');
		$this->form_validation->set_rules('PostContent', 'Post Content', 'required|xss_clean');
		$this->form_validation->set_rules('PostCategory', 'Post Category', 'required|xss_clean');
		$this->form_validation->set_rules('PostAuthor', 'Post Author', 'required|xss_clean');
		
		if ($this->form_validation->run() == FALSE){
			$this->response(array('status'=>'FALSE'), 400);
		}
		else {
			$PostAuthor = $this->input->post('PostAuthor');
			$validParams['PostImage'] = $this->photos->create_photo(array('PhotoCategory'=>10, 'PhotoAuthor'=>$PostAuthor));
			$created = $this->posts->create_post($validParams);
			if($created){
				$this->response(array('status'=>TRUE, 'PostId'=>$created), 200);
			}
			else{
				$this->response(array('status'=>FALSE, 'message'=>'Unknown Server error'), 500);
			}
		}
	}

	function posts_put(){
		$params = $this->input->get();
		if(!$params){
			$this->response(array('status'=>FALSE), 400);
		}
		$PostId = $this->uri->segment(2);
		$allowed = array('PostTitle','PostContent','PostCategory','PostImage','PostTags','PostAuthor');
		$validParams = $this->util->checkValidParams($params, $allowed);
		if(!$validParams){
			$this->response(array('status'=>FALSE), 400);	
		}

		$errors = array();
		foreach($_GET as $key=>$value){
			switch ($value) {
				case 'PostTitle':
				{
					$errors[] = is_numeric($value) ? 'numeric' : NULL;
					$errors[] = empty($value) ? 'empty' : NULL;
					continue;
				}
				case 'PostCategory':
				case 'PostAuthor':{
					$errors[] = !is_numeric($value) ? 'numeric' : NULL;
					continue;
				}

				default:
					continue;
			}
		}
		if(count($errors)){
			$this->response(array('status'=>$errors), 400);
		}

		$updated = $this->posts->update_post($PostId, $validParams);
		if($updated === TRUE){
			$this->response(array('status'=>TRUE), 200);
		}
		else{
			$this->response(array('status'=>FALSE), $updated);
		}
	}
	
	function posts_delete(){
		$PostId = $this->uri->segment(2);
		if(!$PostId || !is_numeric($PostId)){
			$this->response(array('status'=>FALSE), 400);
		}
		else{
			$deleted = $this->posts->delete_post($PostId);
			if($deleted === TRUE){
				$this->response(array('status'=>TRUE), 200);
			}
			else{
				$this->response(array('status'=>FALSE), $deleted);
			}
		}
	}
}