<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Menu extends REST_Controller
{
	function __construct(){
		parent::__construct();
		$this->load->model('Menus_model', 'menus');
 		$this->load->model('Utility_model', 'util');
	}

	/*
	 *	
	 */
	function menus_get(){
		$filters = $this->input->get();
		if($filters){
			$allowed_filters = array('MenuId','MenuName','MenuParent','MenuPosition','fields','callback','_');
			$validFilters = $this->util->checkValidParams($filters, $allowed_filters);
			if(!$validFilters){
				$this->response(array('status'=>FALSE), 400);
			}
			else{
				//TODO: Move to a parameter on get_users()
				if(isset($validFilters['fields'])) unset($validFilters['fields']);
				if(isset($validFilters['callback'])) unset($validFilters['callback']);
				if(isset($validFilters['_'])) unset($validFilters['_']);
				$this->db->where($validFilters);
			}
		}

		$fields = $this->input->get('fields');
		if($fields){
			$fields = explode(',',$fields);
			$allowed_fields = array('MenuId','MenuName','MenuParent','MenuPosition','MenuCreated');
			$validFields = $this->util->checkValidParams($fields, $allowed_fields);
			if(!$validFields){
				$this->response(array('status'=>FALSE), 400);
			}
			else{
				//TODO: Move this to a parameter on get_users()
				$this->db->select($validFields);
			}
		}

		$menus = $this->menus->get_menus();
		if($menus){
			$this->response(array('status'=>TRUE, 'menus'=>$menus), 200);
		}
		else{
			$this->response(array('status'=>TRUE, 'menus'=>'No Menus'), 204);	
		}
	}


	/*
	 *	
	 */
	function menus_post(){
		$allowed = array('MenuName','MenuParent','MenuPosition');
		$params = $this->input->post();
		$validParams = $this->util->checkValidParams($params, $allowed);
		if(!$validParams){
			$this->response(array('status'=>FALSE), 400);	
		}

		$this->load->library('form_validation');
		$this->form_validation->set_rules('MenuName', 'Menu Name', 'required|xss_clean');
		
		if ($this->form_validation->run() == FALSE){
			$this->response(array('status'=>FALSE), 400);
		}
		else {
			$created = $this->menus->create_menu($validParams);
			if($created){
				$this->response(array('status'=>TRUE, 'MenuId'=>$created), 200);
			}
			else{
				$this->response(array('status'=>FALSE, 'message'=>'Unknown Server error'), 500);
			}
		}
	}
	
	function menus_delete(){
		$MenuId = $this->uri->segment(2);
		if(!$MenuId || !is_numeric($MenuId)){
			$this->response(array('status'=>FALSE), 400);
		}
		else{
			$deleted = $this->menus->delete_menu($MenuId);
			if($deleted === TRUE){
				$this->response(array('status'=>TRUE), 200);
			}
			else{
				$this->response(array('status'=>FALSE), $deleted);
			}
		}
	}
}