<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Auth extends REST_Controller
{
	function __construct(){
		parent::__construct();
		$this->load->model('Users_model', 'users');
		$this->load->model('Roles_model', 'roles');
	}

	/*
	 *	Authenticate a user
	 *	@param Email
	 *	@param Pass
	 *
	 *	@return authToken
	 */
	function auth_post(){

	}

	function signout_get(){

	}

	/*
	 *	@param Email
	 *	Sends an email to a user to assisti
	 */
	function forgot_post(){

	}
}