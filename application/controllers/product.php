<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Product extends REST_Controller
{
	function __construct(){
		parent::__construct();
		$this->load->model('Products_model', 'products');
 		$this->load->model('Utility_model', 'util');
	}

	/*
	 *	
	 */
	function products_get(){
		$filters = $this->input->get();
		if($filters){
			$allowed_filters = array('ProductId','ProductTitle','ProductContent','ProductCategory','ProductImage','ProductAuthor','ProuductTags','fields');
			$validFilters = $this->util->checkValidParams($filters, $allowed_filters);
			if(!$validFilters){
				$this->response(array('status'=>FALSE), 400);
			}
			else{
				//TODO: Move to a parameter on get_users()
				if(isset($validFilters['fields'])) unset($validFilters['fields']);
				$this->db->where($validFilters);
			}
		}

		$fields = $this->input->get('fields');
		if($fields){
			$fields = explode(',',$fields);
			$allowed_fields = array('ProductId','ProductTitle','ProductContent','ProductCategory','ProductImage','ProductAuthor','ProuductTags','ProductCreated');
			$validFields = $this->util->checkValidParams($fields, $allowed_fields);
			if(!$validFields){
				$this->response(array('status'=>FALSE), 400);
			}
			else{
				//TODO: Move this to a parameter on get_users()
				$this->db->select($validFields);
			}
		}

		$products = $this->products->get_products();
		if($products){
			$this->response(array('status'=>TRUE, 'products'=>$products), 200);
		}
		else{
			$this->response(array('status'=>TRUE, 'products'=>'No Products'), 204);	
		}
	}


	/*
	 *	
	 */
	function products_post(){
		$allowed = array('ProductTitle','ProductContent','ProductCategory','ProductImage','ProductAuthor','ProductTags');
		$params = $this->input->post();
		$validParams = $this->util->checkValidParams($params, $allowed);
		if(!$validParams){
			$this->response(array('status'=>FALSE), 400);	
		}

		$this->load->library('form_validation');
		$this->form_validation->set_rules('ProductTitle', 'ProductTitle', 'required|xss_clean');
		
		if ($this->form_validation->run() == FALSE){
			$this->response(array('status'=>FALSE), 400);
		}
		else {
			$created = $this->products->create_product($validParams);
			if($created){
				$this->response(array('status'=>TRUE, 'ProductId'=>$created), 200);
			}
			else{
				$this->response(array('status'=>FALSE, 'message'=>'Unknown Server error'), 500);
			}
		}
	}
	
	function products_delete(){
		$ProductId = $this->uri->segment(2);
		if(!$ProductId || !is_numeric($ProductId)){
			$this->response(array('status'=>FALSE), 400);
		}
		else{
			$deleted = $this->products->delete_product($ProductId);
			if($deleted === TRUE){
				$this->response(array('status'=>TRUE), 200);
			}
			else{
				$this->response(array('status'=>FALSE), $deleted);
			}
		}
	}
}