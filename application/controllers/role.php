<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Role extends REST_Controller
{
	function __construct(){
		parent::__construct();
		$this->load->model('Roles_model', 'roles');
		$this->load->model('Utility_model', 'util');
	}

	function roles_get(){
		$roles = $this->roles->get_roles();
		if($roles){
			$this->response(array('status'=>TRUE, 'roles'=>$roles), 200);
		}
		else{
			$this->response(array('status'=>TRUE, 'roles'=>'No Roles'), 204);	
		}
	}

	function roles_post(){		
		$allowed = array('RoleName');
		$params = $this->input->post();
		$validParams = $this->util->checkValidParams($params, $allowed);
		if(!$validParams){
			$this->response(array('status'=>FALSE), 400);	
		}

		$this->load->library('form_validation');
		$this->form_validation->set_rules('RoleName', 'RoleName', 'required|xss_clean');
		if ($this->form_validation->run() == FALSE){
			$this->response(array('status'=>FALSE), 400);
		}
		else {
			$created = $this->roles->create_role($validParams);
			if($created){
				$this->response(array('status'=>TRUE, 'RoleId'=>$created), 200);
			}
			else{
				$this->response(array('status'=>FALSE, 'message'=>'Unknown Server error'), 500);
			}
		}
	}

	function roles_delete(){
		$RoleId = $this->uri->segment(2);
		if(!$RoleId || !is_numeric($RoleId)){
			$this->response(array('status'=>FALSE), 400);
		}
		else{
			$deleted = $this->roles->delete_role($RoleId);
			if($deleted === TRUE){
				$this->response(array('status'=>TRUE), 200);
			}
			else{
				$this->response(array('status'=>FALSE), $deleted);
			}
		}
	}
}