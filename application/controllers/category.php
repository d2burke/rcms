<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Category extends REST_Controller
{
	function __construct(){
		parent::__construct();
		$this->load->model('categories_model', 'categories');
 		$this->load->model('Utility_model', 'util');
	}

	/*
	 *	
	 */
	function categories_get(){
		$filters = $this->input->get();
		if($filters){
			$allowed_filters = array('CategoryName','fields','callback','_');
			$validFilters = $this->util->checkValidParams($filters, $allowed_filters);
			if(!$validFilters){
				$this->response(array('status'=>FALSE), 400);
			}
			else{
				//TODO: Move to a parameter on get_users()
				if(isset($validFilters['fields'])) unset($validFilters['fields']);
				if(isset($validFilters['callback'])) unset($validFilters['callback']);
				if(isset($validFilters['_'])) unset($validFilters['_']);
				$this->db->where($validFilters);
			}
		}

		$fields = $this->input->get('fields');
		if($fields){
			$fields = explode(',',$fields);
			$allowed_fields = array('CategoryId','CategoryName');
			$validFields = $this->util->checkValidParams($fields, $allowed_fields);
			if(!$validFields){
				$this->response(array('status'=>FALSE), 400);
			}
			else{
				//TODO: Move this to a parameter on get_users()
				$this->db->select($validFields);
			}
		}

		$categories = $this->categories->get_categories();
		if($categories){
			$this->response(array('status'=>TRUE, 'categories'=>$categories), 200);
		}
		else{
			$this->response(array('status'=>TRUE, 'categories'=>'No Categories'), 204);	
		}
	}


	/*
	 *	
	 */
	function categories_post(){
		$allowed = array('CategoryName');
		$params = $this->input->post();
		$validParams = $this->util->checkValidParams($params, $allowed);
		if(!$validParams){
			$this->response(array('status'=>FALSE), 400);	
		}

		$this->load->library('form_validation');
		$this->form_validation->set_rules('CategoryName', 'Category Name', 'required|xss_clean');
		
		if ($this->form_validation->run() == FALSE){
			$this->response(array('status'=>FALSE), 400);
		}
		else {
			$created = $this->categories->create_category($validParams);
			if($created){
				$this->response(array('status'=>TRUE, 'CategoryId'=>$created), 200);
			}
			else{
				$this->response(array('status'=>FALSE, 'message'=>'Unknown Server error'), 500);
			}
		}
	}
	
	function categories_delete(){
		$CategoryId = $this->uri->segment(2);
		if(!$CategoryId || !is_numeric($CategoryId)){
			$this->response(array('status'=>FALSE), 400);
		}
		else{
			$deleted = $this->categories->delete_category($CategoryId);
			if($deleted === TRUE){
				$this->response(array('status'=>TRUE), 200);
			}
			else{
				$this->response(array('status'=>FALSE), $deleted);
			}
		}
	}
}