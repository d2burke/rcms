<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class Photo extends REST_Controller
{
	function __construct(){
		parent::__construct();
 		$this->load->model('Photos_model', 'photos');
 		$this->load->model('Utility_model', 'util');
	}

	/*
	 *	Get all photos, or pass filters
	 */
	function photos_get(){
		$filters = $this->input->get();
		$PhotoId = $this->uri->segment(2);
		$validFilters = array();
		if($filters){
			$allowed_filters = array('PhotoId','PhotoFileName','PhotoAuthor','PhotoCategory', 'PhotoCreated');
			$validFilters = $this->util->checkValidParams($filters, $allowed_filters);
			if(!$validFilters){
				$this->response(array('status'=>FALSE), 400);
			}
		}
		$photos = $this->photos->get_photos($validFilters);
		if($photos){
			$this->response(array('status'=>TRUE, 'photos'=>$photos), 200);
		}
		else{
			$this->response(array('status'=>TRUE, 'photos'=>'No Photos'), 204);	
		}
	}

	function photos_post(){
		$allowed_fields = array('PhotoFileName','PhotoCaption','PhotoAuthor','PhotoCategory');
		$params = $this->input->post();
		$validParams = $this->util->checkValidParams($params, $allowed_fields);
		if(!$validParams){
			$this->response(array('status'=>FALSE), 400);	
		}
		$created = $this->photos->create_photo($validParams);
		if($created){
			$this->response(array('status'=>TRUE, 'PhotoId'=>$created), 200);
		}
		else{
			$this->response(array('status'=>FALSE, 'message'=>'Unknown Server error'), 500);
		}
	}
	
	function photos_delete(){
		$PhotoId = $this->uri->segment(2);
		if(!$PhotoId || !is_numeric($PhotoId)){
			$this->response(array('status'=>FALSE), 400);
		}
		else{
			$deleted = $this->photos->delete_photo($PhotoId);
			if($deleted === TRUE){
				$this->response(array('status'=>TRUE), 200);
			}
			else{
				$this->response(array('status'=>FALSE), $deleted);
			}
		}
	}
}