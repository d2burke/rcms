<?php defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH.'/libraries/REST_Controller.php';

class User extends REST_Controller
{
	function __construct(){
		parent::__construct();
		$this->load->model('Users_model', 'users');
		$this->load->model('Photos_model', 'photos');
 		$this->load->model('Utility_model', 'util');
	}

	/*
	 *	Get all users, or pass filters
	 *	TODO: Pagination
	 */
	function users_get(){
		$filters = $this->input->get();
		$UserId = $this->uri->segment(2);
		if($filters){
			$allowed_filters = array('UserId','Fname','Lname','Email','UserCreated','fields','callback','_');
			$validFilters = $this->util->checkValidParams($filters, $allowed_filters);
			if(!$validFilters){
				$this->response(array('status'=>FALSE), 400);
			}
			else{
				//TODO: Move to a parameter on get_users()
				if(isset($validFilters['fields'])) unset($validFilters['fields']);
				if(isset($validFilters['callback'])) unset($validFilters['callback']);
				if(isset($validFilters['_'])) unset($validFilters['_']);
				$this->db->where($validFilters);
			}
		}

		$fields = $this->input->get('fields');
		if($fields){
			$fields = explode(',',$fields);
			$allowed_fields = array('UserId','Fname','Lname','Email','UserImage','FacebookId','APNSToken','UserCreated');
			$validFields = $this->util->checkValidParams($fields, $allowed_fields);
			if(!$validFields){
				$this->response(array('status'=>FALSE), 400);
			}
			else{
				//TODO: Move this to a parameter on get_users()
				$this->db->select($validFields);
			}
		}

		$users = $this->users->get_users($UserId);
		if($users){
			$this->response(array('status'=>TRUE, 'users'=>$users), 200);
		}
		else{
			$this->response(array('status'=>TRUE, 'users'=>'No Users'), 204);	
		}
	}

	/*
	 *	Check for unwanted parameters, then validation their values.
	 *	If these tests pass, unset PassConf[irmation] and call the 
	 *	users model to insert the data.
	 *
	 *	If they fail, send a Malformed Request code.
	 */
	function users_post(){
		$allowed = array('Fname','Lname','Email','Pass','PassConf','UserImage','FacebookId','APNSToken');
		$params = $this->input->post();
		$validParams = $this->util->checkValidParams($params, $allowed);
		if(!$validParams){
			$this->response(array('status'=>FALSE), 400);	
		}

		$this->load->library('form_validation');
		$this->form_validation->set_rules('Email', 'Email', 'required|xss_clean|valid_email');
		$this->form_validation->set_rules('Pass', 'Pass', 'required|xss_clean|sha1');
		$this->form_validation->set_rules('PassConf', 'Password Confirmation', 'required|xss_clean|matches[Pass]|sha1');
		
		if ($this->form_validation->run() == FALSE){
			$this->response(array('status'=>FALSE), 400);
		}
		else {
			if(isset($validParams['PassConf'])) unset($validParams['PassConf']);
			$validParams['UserImage'] = $this->photos->create_photo(array('PhotoCategory'=>1, 'PhotoAuthor'=>1));
			$validParams['Pass'] = sha1($validParams['Pass']);
			$created = $this->users->create_user($validParams);
			if($created){
				$this->response(array('status'=>TRUE, 'UserId'=>$created), 200);
			}
			else{
				$this->response(array('status'=>FALSE, 'message'=>'Unknown Server error'), 500);
			}
		}
	}

	function users_put(){
		$params = $this->input->get();
		$UserId = $this->uri->segment(2);
		$allowed = array('Fname','Lname','Email','Pass','PassConf','UserImage','FacebookId','APNSToken');
		$validParams = $this->util->checkValidParams($params, $allowed);
		if(!$validParams){
			$this->response(array('status'=>FALSE), 400);	
		}

		$errors = array();
		foreach($_GET as $key=>$value){
			switch ($value) {
				case 'Fname':
				case 'Lname':
				{
					$errors[] = is_numeric($value) ? 'numeric' : NULL;
					$errors[] = empty($value) ? 'empty' : NULL;
					continue;
				}
				case 'Email':{
					$erros[] = filter_var($value, FILTER_VALIDATE_EMAIL) ? 'bad email' : NULL;
					continue;
				}
				
				case 'Pass':{
					$errors[] = empty($value) ? 'empty' : NULL;
					$errors[] = strlen($value) < 6 ? 'too short' : NULL;
					continue;
				}
				
				case 'PassConf':{
					$errors = $value !== $_GET['Pass'] ? 'mismatch' : NULL;
					continue;
				}

				default:
					continue;
			}
		}
		if(count($errors)){
			$this->response(array('status'=>$errors), 400);
		}

		if(isset($validParams['PassConf'])) unset($validParams['PassConf']);
		$updated = $this->users->update_user($UserId, $validParams);
		if($updated === TRUE){
			$this->response(array('status'=>TRUE), 200);
		}
		else{
			$this->response(array('status'=>FALSE), $updated);
		}
	}
	
	function users_delete(){
		$UserId = $this->uri->segment(2);
		if(!$UserId || !is_numeric($UserId)){
			$this->response(array('status'=>FALSE), 400);
		}
		else{
			$deleted = $this->users->delete_user($UserId);
			if($deleted === TRUE){
				$this->response(array('status'=>TRUE), 200);
			}
			else{
				$this->response(array('status'=>FALSE), $deleted);
			}
		}
	}
}