module.exports = (function(grunt) {
    'use strict';

    grunt.initConfig({
        'ftp-deploy': {
          build: {
            auth: {
              host: 'tantopic.com',
              port: 21,
              authKey: 'key1'
            },
            src: '/Users/danielburke/Documents/Development/D2/Sites/cms',
            dest: 'domains/d2burke.com/html/dev/cms',
            exclusions: ['/Users/danielburke/Documents/Development/D2/Sites/cms/.DS_Store',
                        '/Users/danielburke/Documents/Development/D2/Sites/cms/.git',
                        '/Users/danielburke/Documents/Development/D2/Sites/cms/.gitignore',
                        '/Users/danielburke/Documents/Development/D2/Sites/cms/.ftppass',
                        '/Users/danielburke/Documents/Development/D2/Sites/cms/package.json',
                        '/Users/danielburke/Documents/Development/D2/Sites/cms/Gruntfile.js',
                        '/Users/danielburke/Documents/Development/D2/Sites/cms/system',
                        '/Users/danielburke/Documents/Development/D2/Sites/cms/application/.DS_Store',
                        '/Users/danielburke/Documents/Development/D2/Sites/cms/controllers/.DS_Store',
                        '/Users/danielburke/Documents/Development/D2/Sites/cms/application/cache',
                        '/Users/danielburke/Documents/Development/D2/Sites/cms/application/core',
                        '/Users/danielburke/Documents/Development/D2/Sites/cms/application/errors',
                        '/Users/danielburke/Documents/Development/D2/Sites/cms/application/helpers',
                        '/Users/danielburke/Documents/Development/D2/Sites/cms/application/hooks',
                        '/Users/danielburke/Documents/Development/D2/Sites/cms/application/language',
                        '/Users/danielburke/Documents/Development/D2/Sites/cms/application/libraries',
                        '/Users/danielburke/Documents/Development/D2/Sites/cms/application/config',
                        '/Users/danielburke/Documents/Development/D2/Sites/cms/application/logs',
                        '/Users/danielburke/Documents/Development/D2/Sites/cms/application/third_party',
                        '/Users/danielburke/Documents/Development/D2/Sites/cms/application/index.html',
                        '/Users/danielburke/Documents/Development/D2/Sites/cms/application/views/index.html',
                        '/Users/danielburke/Documents/Development/D2/Sites/cms/application/controllers/index.html',
                        '/Users/danielburke/Documents/Development/D2/Sites/cms/application/models/index.html',
                        '/Users/danielburke/Documents/Development/D2/Sites/cms/controllers/.DS_Store',
                        '/Users/danielburke/Documents/Development/D2/Sites/cms/node_modules']
          }
        }
    });

    grunt.loadNpmTasks('grunt-ftp-deploy');

    grunt.registerTask('default', [
        'ftp-deploy'
    ]);
});